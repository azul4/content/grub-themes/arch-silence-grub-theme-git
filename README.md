# arch-silence-grub-theme-git

Arch Silence - GRUB2 theme - GIT version

https://github.com/fghibellini/arch-silence

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/grub-themes/arch-silence-grub-theme-git.git
```

<br><br>

## Screen preview:

<p align="center">
<img src="https://gitlab.com/azul4/images/arch-silence-grub-theme-git/-/raw/main/arch-silence.png">
</p>



